Hotels::Application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, path: '/', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }

  namespace :catalogue do
    get 'dashboard/index'
    resources :raitings, only: [:create, :update]
    resources :comments, only: :create
    resources :hotels, only: [:index, :show, :create, :new]
  end

  root to: 'catalogue/dashboard#index'
end
