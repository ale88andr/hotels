module HotelHelper
  def rating_score
    if @rating = current_user.raitings.find_by_hotel_id(params[:id])
      @rating
    else
      current_user.raitings.new
    end
  end

  def current_user_rating
    if @rating = current_user.raitings.find_by_hotel_id(params[:id])
      @rating.score
    else
      'N/A'
    end
  end
end