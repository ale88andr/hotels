class Catalogue::HotelsController < Catalogue::ApplicationController
  inherit_resources
  actions :index, :show, :new, :create

  def create
    @hotel = current_user.hotels.build(hotel_params)
    create! do |success, failure|
      success.html {
        redirect_to catalogue_hotels_url,
        notice: 'Your hotel added. Expect to administrator confirm your request'
      }
      failure.html {
        flash[:error] = 'Adding aborted! Check your hotel data'
        render action: 'new'
      }
    end
  end

  protected
    def collection
      @hotels ||= end_of_association_chain.approved
    end

    def hotel_params
      params.require(:hotel).permit!.except(:author_id)
    end
end
