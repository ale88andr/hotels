class Catalogue::RaitingsController < Catalogue::ApplicationController
  before_action :set_hotel, only: [:create, :update]

  def create
    if current_user.id == @hotel.author.id
      redirect_to catalogue_hotel_path(@hotel), alert: 'You cannot rate for your own hotel'
    else
      @rating = Raiting.new(raiting_params)
      @rating.hotel_id = @hotel.id
      @rating.user_id = current_user.id
      if @rating.save
        respond_to do |format|
          format.html { redirect_to catalogue_hotel_path(@hotel), notice: 'Your rating has been saved' }
          format.js
        end
      end
    end
  end

  def update
    if current_user.id == @hotel.author.id
      redirect_to catalogue_hotel_url(@hotel), alert: 'You cannot rate for your own hotel'
    else
      @rating = current_user.raitings.find_by_hotel_id(@hotel.id)
      if @rating.update_attributes(raiting_params)
        respond_to do |format|
          format.html { redirect_to catalogue_hotel_url(@hotel), notice: 'Your rating has been updated' }
          format.js
        end
      end
    end
  end

  protected

  def raiting_params
    params.require(:raiting).permit!.except(:user_id)
  end

  def set_hotel
    @hotel = Hotel.find_by_id(raiting_params[:hotel_id])
  end
end
