class Catalogue::CommentsController < Catalogue::ApplicationController
  inherit_resources
  actions :new, :create

  def create
    @comment = current_user.comments.build(comment_params)
    create! do |success, failure|
      success.html {
        redirect_to catalogue_hotel_url(comment_params[:hotel_id]),
                    notice: 'Your comment added.'
      }
      failure.html {
        flash[:error] = 'Adding comment aborted! wright something, please!'
        redirect_to catalogue_hotel_url(comment_params[:hotel_id])
      }
    end
  end

  protected

  def comment_params
    params.require(:comment).permit!.except(:user_id)
  end
end
