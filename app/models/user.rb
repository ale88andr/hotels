class User < ActiveRecord::Base
  has_many :hotels, foreign_key: 'author_id'
  has_many :comments
  has_many :raitings
  has_many :rated_hotels, through: :raitings, source: :hotel

  devise :database_authenticatable, :registerable,
          :rememberable, :trackable, :validatable
end
