class Hotel < ActiveRecord::Base
  STATUSES = { default: 'pending', approved: 'approved', rejected: 'rejected' }
  serialize :address, Hash

  belongs_to :author, class_name: 'User', foreign_key: 'author_id', counter_cache: true
  has_many   :comments, include: :user
  has_many   :raitings
  has_many   :raters, through: :raitings, source: :user

  validates :title,
            presence: true,
            uniqueness: { case_sensitive: false},
            length: { maximum: 75 }
  validates :price_for_room,
            numericality: { greater_than: 0 },
            allow_nil: true
  validates :status,
            inclusion: { in: STATUSES.values },
            allow_blank: false,
            on: :update

  scope :approved,  -> { where(status: 'approved') }
  scope :rejected,  -> { where(status: 'rejected') }
  scope :pending,   -> { where(status: 'pending') }

  before_create :set_default_status

  mount_uploader :photo, PhotoUploader

  def set_default_status
    self.status = STATUSES[:default]
  end

  def average_rating
    @rate = 0
    self.raitings.each do |raiting|
      @rate += raiting.score
    end
    @total = self.raitings.size
    @rate.to_f / @total.to_f
  end

end
