class Comment < ActiveRecord::Base
  belongs_to :user, counter_cache: true
  belongs_to :hotel

  validates :body, presence: true
end