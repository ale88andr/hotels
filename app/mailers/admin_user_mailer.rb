class AdminUserMailer < ActionMailer::Base
  default from: 'hotels@admin.com'

  def status_changed(user, hotel)
    @recipient = user
    @url = catalogue_hotel_url(hotel)
    mail(to: @recipient.email, subject: "Your hotel #{hotel.title} moderated")
  end
end
