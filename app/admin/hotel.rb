ActiveAdmin.register Hotel do

  # Pagination index page
  config.per_page = 20

  # Permission
  permit_params :status, :title, :room_description, :price_for_room, :breakfast, :author_id, :photo

  # Scopes
  scope :approved
  scope :rejected
  scope :pending

  # Filters
  filter  :author,
          as:           :select,
          collection:   User.all.map{ |u| [u.email,u.id] },
          label:        'Select Author'
  filter  :title,       label: 'Title'
  filter  :status,
          as:           :select,
          collection:   %w[pending approved rejected],
          label:        'Status'
  filter  :breakfast,
          as:           :select
  filter  :created_at,  label: 'Дата создания'

  # Controller
  controller do
    def update
      update! do |format|
        AdminUserMailer.status_changed(@hotel.author, @hotel).deliver if @hotel.previous_changes.include?('status')
      end
    end

    def scoped_collection
      Hotel.includes(:author)
    end
  end

  # Index page
  index do
    column  'Hotel title',
            :title,
            sortable: true
    column 'Hotel status', sortable: :status do |h|
      status_tag h.status, :ok
    end
    column 'Breakfast', :breakfast
    column 'Added',
           :created_at,
           sortable: true
    column 'Author' do |h|
      h.author.present? ? link_to(h.author.email, admin_user_path(h.author)) : 'Unknown'
    end

    actions
  end

  form do |f|
    f.inputs 'Adding hotel' do
      f.input :author_id,
              as:             :select,
              collection:     User.all.map{ |u| [u.email,u.id] },
              include_blank:  false,
              label:          'Author:',
              hint:           'Select author of hotel publication.'
      f.input :title,
              label:          'Hotel title',
              hint:           'Type title of hotel',
              required:       true
      f.input :room_description,
              label:          'Hotel room description',
              hint:           'Type description'
      f.input :price_for_room,
              label:          'Price',
              hint:           'Type price for room',
              type:           'number'
      f.input :breakfast,
              label:          'Breakfast?'
      f.input :photo,
              as:             :file,
              label:          'Photo',
              hint:           'Select photo of thr room'
      unless f.object.new_record?
        if f.object.status == 'pending'
          f.input :status,
                  as:            :select,
                  collection:    options_for_select(Hotel::STATUSES.values, f.object.status),
                  include_blank: false
        else
          f.input :status,
                  input_html:   { disabled: true }
        end
      end

      f.actions
    end
  end

end
