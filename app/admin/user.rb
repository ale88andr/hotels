ActiveAdmin.register User do

  # Pagination index page
  config.per_page = 15

  # Filters
  filter :email

  permit_params :email,
                :password,
                :password_confirmation

  # Index page
  index do
    column  'Email',
            :email,
            sortable: true
    column 'Added Hotels', sortable: :true do |u|
      u.hotels.size
    end
    column 'Comments', sortable: :true do |u|
      u.comments.size
    end
    column 'Rated hotels', sortable: :true do |u|
      u.raitings.size
    end
    column  'Registered at',
            :created_at

    actions
  end

  form do |f|
    f.inputs 'Add user' do
      f.input :email,
              label:  'email',
              required: true,
              hint:   'type user email'
      f.input :password,
              label:  'password',
              required: true,
              hint:   'type user password'
      f.input :password_confirmation,
              label:  'confirm password',
              required: true,
              hint:   'retype user password'
    end
    f.actions
  end
end
