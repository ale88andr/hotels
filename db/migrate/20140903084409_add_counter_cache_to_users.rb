class AddCounterCacheToUsers < ActiveRecord::Migration
  def change
    add_column :users, :comments_count, :integer, default: 0, null: false
    add_column :users, :hotels_count,   :integer, default: 0, null: false
    add_column :users, :raitings_count, :integer, default: 0, null: false
  end
end
