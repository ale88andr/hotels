class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.string      :title
      t.boolean     :breakfast
      t.string      :photo
      t.integer     :price_for_room
      t.text        :room_description
      t.string      :address
      t.string      :status
      t.belongs_to  :author, index: true

      t.timestamps
    end

    add_index :hotels, :title, unique: true
  end
end
