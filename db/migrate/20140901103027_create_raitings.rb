class CreateRaitings < ActiveRecord::Migration
  def change
    create_table :raitings do |t|
      t.belongs_to :user, index: true
      t.belongs_to :hotel, index: true
      t.integer :score

      t.timestamps
    end
  end
end
