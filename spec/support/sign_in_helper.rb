module SignInHelper
  def auth(user=nil)
    @user = user
    @user ||= create :user
    visit new_user_session_path
    fill_in 'user[email]',    with: @user.email
    fill_in 'user[password]', with: @user.password
    click_on 'Log in'
  end

  alias_method :login, :auth
end