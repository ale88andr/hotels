# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    body    { Faker::Lorem.sentence }

    trait :full do
      association :user,  factory: :user
      association :hotel, factory: :hotel
    end
  end
end
