# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :hotel do
    title             { Faker::Company.name }
    breakfast         false
    photo             'path/to/photo'
    price_for_room    1
    room_description  { Faker::Lorem.paragraph }
    address           {{
        street:   Faker::Address.street_address,
        city:     Faker::Address.city,
        country:  Faker::Lorem.word
    }}

    trait :with_author do
      association :author, factory: :user
    end
  end
end
