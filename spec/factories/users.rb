# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    email                 { Faker::Internet.email }
    password              'Pa$$w0rd'
    password_confirmation 'Pa$$w0rd'
  end
end
