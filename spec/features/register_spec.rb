require 'rails_helper'

describe 'follow register steps:' do
  Given(:user) { attributes_for :user }
  Given { visit '/register' }

  context 'visit register form' do
    Then do
      within('#new_user') do
        expect(page).to have_field 'user[email]',                 type: 'email'
        expect(page).to have_field 'user[password]',              type: 'password'
        expect(page).to have_field 'user[password_confirmation]', type: 'password'
      end
    end
  end

  context 'fill in form' do

    context 'with valid data' do
      Given do
        within('#new_user') do
          fill_in 'user_email',                 with: user[:email]
          fill_in 'user_password',              with: user[:password]
          fill_in 'user_password_confirmation', with: user[:password]
        end
      end
      When { click_on 'Create user' }

      context 'redirect' do
        Then { expect(current_path).to eq root_path }
      end

      context 'sets flash message' do
        Then { expect(page).to have_selector 'div', text: 'Welcome! You have signed up successfully.' }
      end

      context 'show link to logout' do
        Then { expect(page).to have_link "Logout (#{user[:email]})" }
      end

      context 'show user email' do
        Then { expect(page).to have_content user[:email] }
      end
    end

    context 'with invalid data' do
      Given do
        within('#new_user') do
          fill_in 'user_email',                 with: nil
          fill_in 'user_password',              with: user[:password]
          fill_in 'user_password_confirmation', with: user[:password]
        end
      end
      When { click_on 'Create user' }

      context 'error message' do
        Then { expect(page).to have_content "Email can't be blank" }
      end
    end
  end
end