require 'rails_helper'

describe 'GET show' do
  Given!(:hotel) { create :hotel, :with_author }
  Given { hotel.update_attribute :status, 'approved' }

  describe 'view hotel' do
    When  { visit catalogue_hotel_url(hotel) }

    Then { expect(page).to have_selector 'h1', text: hotel.title }
    Then { expect(page).to have_selector 'h3.text-success',
                                         text: hotel.price_for_room }
    Then { expect(page).to have_selector 'p', text: hotel.created_at }
    Then { expect(page).to have_selector 'p', text: hotel.author.email }
  end

  describe 'view hotel rate' do
    context 'user access' do
      Given { login }
      When  { visit catalogue_hotel_url(hotel) }

      Then { expect(page).to have_selector 'table', text: 'Hotel Rating' }
      Then { expect(page).to have_selector "form#edit_raiting_#{hotel.id}" }
    end

    context 'guest access' do
      When  { visit catalogue_hotel_url(hotel) }

      Then { expect(page).not_to have_selector 'table', text: 'Hotel Rating' }
      Then { expect(page).not_to have_selector "form#new_raiting_#{hotel.id}" }
    end
  end

  describe 'hotel comments' do
    context 'show comments' do
      Given!(:comment) { create :comment, hotel_id: hotel.id, user_id: hotel.author.id }
      When  { visit catalogue_hotel_url(hotel) }

      Then { expect(page).to have_selector 'h2', text: 'Comments' }
      Then { expect(page).to have_content comment.body }
    end

    context 'add comment user access' do
      Given { login }
      When  { visit catalogue_hotel_url(hotel) }

      Then { expect(page).to have_selector "form#new_comment" }
      Then { expect(page).to have_field 'comment[body]' }
      Then { expect(page).to have_button 'Comment' }
    end

    context 'add comment guest access' do
      When  { visit catalogue_hotel_url(hotel) }

      Then { expect(page).not_to have_content '\"Please sign in or register to leave comment for this hotel\"' }
      Then { expect(page).not_to have_selector "form#new_comment" }
    end
  end

end
