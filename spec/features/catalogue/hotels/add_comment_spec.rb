require 'rails_helper'

describe 'creating comment' do
  Given!(:hotel) { create :hotel, :with_author }
  Given { hotel.update_attribute :status, 'approved' }
  Given { login }
  Given { visit catalogue_hotel_path(hotel) }

  context 'with comment body' do
    When do
      within('#new_comment') do
        fill_in 'comment[body]', with: 'Some text'
      end
    end

    Then { expect{ click_on 'Comment' }.to change(Comment, :count).by(1) }
  end

  context 'without comment body' do
    When do
      within('#new_comment') do
        fill_in 'comment[body]', with: nil
      end
    end

    Then { expect{ click_on 'Comment' }.not_to change(Comment, :count) }
  end
end