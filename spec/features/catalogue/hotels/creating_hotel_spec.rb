require 'rails_helper'

describe 'follow creating hotel steps' do
  Given { login }

  context 'visiting add hotel form' do
    Given { visit root_path }

    context 'expecting add hotel link' do
      Then { expect(page).to have_link 'Add hotel', href: new_catalogue_hotel_path }
    end

    context 'click add hotel button' do
      When { click_link 'Add hotel' }
      Then do
        within('#new_hotel') do
          expect(page).to have_field 'hotel[title]',            type: 'text'
          expect(page).to have_field 'hotel[room_description]'
          expect(page).to have_field 'hotel[breakfast]',        type: 'checkbox'
          expect(page).to have_field 'hotel[price_for_room]',   type: 'number'
          expect(page).to have_field 'hotel[photo]',            type: 'file'

          expect(page).to have_field 'hotel[address][country]', type: 'text'
          expect(page).to have_field 'hotel[address][city]',    type: 'text'
          expect(page).to have_field 'hotel[address][street]',  type: 'text'
        end
      end
    end

    context 'fill in new hotel form' do
      Given { visit new_catalogue_hotel_path }

      context 'with valid data' do
        Given(:hotel) { attributes_for :hotel }
        When do
          within('#new_hotel') do
            fill_in 'hotel_title',            with: hotel[:title]
            fill_in 'hotel_room_description', with: hotel[:room_description]
            fill_in 'hotel_price_for_room',   with: hotel[:price_for_room]
            check   'hotel_breakfast'

            fill_in 'hotel_address_street',   with: hotel[:address][:street]
            fill_in 'hotel_address_country',  with: hotel[:address][:country]
            fill_in 'hotel_address_city',     with: hotel[:address][:city]
          end
        end
        Then { expect{ click_button 'Add hotel' }.to change(Hotel, :count).by(1) }
      end

      context 'with invalid data' do
        When do
          within('#new_hotel') do
            fill_in 'hotel_title', with: nil
          end
        end
        Then { expect{ click_button 'Add hotel' }.not_to change(Hotel, :count) }
      end
    end
  end
end