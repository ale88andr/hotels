require 'rails_helper'

describe 'index hotels' do
  Given!(:hotels) { create_list :hotel, 2 }
  Given!(:approved_hotel) { hotels.first }
  Given { approved_hotel.update_attribute :status, 'approved' }
  Given { login }
  When  { visit catalogue_hotels_url }

  context 'shows only approved hotels' do
    Then { expect(page).to have_link approved_hotel.title,
                                     href: catalogue_hotel_path(approved_hotel) }
  end

  context 'do not show pending hotels' do
    Then { expect(page).not_to have_link hotels.last.title,
                                         href: catalogue_hotel_path(hotels.last) }
  end

  context 'shows short description of hotel' do
    Then { expect(page).to have_selector 'h2', text: approved_hotel.title }
    Then { expect(page).to have_selector 'p', text: approved_hotel.room_description }
    Then { expect(page).to have_link 'View Details >>',
                                     href: catalogue_hotel_path(approved_hotel) }
  end
end
