require 'rails_helper'

describe 'follow login steps:' do
  Given(:user) { create :user }

  context 'visit login form' do
    When { visit '/login' }
    Then do
      within('#new_user') do
        expect(page).to have_field 'user[email]',    type: 'email'
        expect(page).to have_field 'user[password]', type: 'password'
      end
    end
  end

  context 'fill in form' do
    Given { visit '/login' }

    context 'with valid data' do
      Given do
        within('#new_user') do
          fill_in 'user_email',     with: user.email
          fill_in 'user_password',  with: user.password
        end
      end
      When { click_on 'Log in' }

      context 'redirect' do
        Then { expect(current_path).to eq root_path }
      end

      context 'sets flash message' do
        Then { expect(page).to have_selector 'div', text: 'Signed in successfully.' }
      end

      context 'show user email' do
        Then { expect(page).to have_text user.email }
      end
    end

    context 'with invalid data' do
      Given do
        within('#new_user') do
          fill_in 'user_email',     with: nil
          fill_in 'user_password',  with: user.password
        end
      end
      When { click_on 'Log in' }

      context 'redirect to login' do
        Then { expect(current_path).to eq '/login' }
      end

      context 'sets flash message' do
        Then { expect(page).to have_selector 'div', text: 'Invalid email or password.' }
      end
    end
  end
end