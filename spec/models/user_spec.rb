require 'rails_helper'

describe User do
  subject { described_class.new }

  context 'database contains' do
    it { expect(subject).to have_db_column(:email).of_type(:string) }
    it { expect(subject).to have_db_column(:encrypted_password).of_type(:string) }
    it { expect(subject).to have_db_index(:email).unique(true) }
  end

  context 'relations' do
    it { expect(subject).to have_many(:hotels).with_foreign_key('author_id') }
    it { expect(subject).to have_many(:comments) }
    it { expect(subject).to have_many(:raitings) }
    it { expect(subject).to have_many(:rated_hotels).through(:raitings) }
  end

  context 'validates' do
    context 'rules' do
      it { expect(subject).to validate_presence_of :email }
      it { expect(subject).to validate_presence_of :password }
      it { expect(subject).to validate_uniqueness_of :email }
      it { expect(subject).to validate_confirmation_of :password }
      it { expect(subject).to ensure_length_of(:password).is_at_least(6) }
    end

    context 'with incorrect data' do
      it { expect(subject).not_to allow_value('example_email').for(:email) }
      it { expect(subject).not_to allow_value('a' * 5).for(:password) }
      it 'duplicate email' do
        user = create(:user)
        expect(subject).not_to allow_value(user.email).for(:email)
      end
    end

    context 'with correct data' do
      it { expect(subject).to allow_value('example@mail.com').for(:email) }
      it { expect(subject).to allow_value('Pa$$w0rd').for(:password) }
      it { expect(build :user).to be_valid }
    end
  end
end
