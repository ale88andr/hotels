require 'rails_helper'

describe Hotel do
  subject { described_class.new }

  describe 'database fields' do
    it { expect(subject).to have_db_column(:title).of_type(:string) }
    it { expect(subject).to have_db_column(:breakfast).of_type(:boolean) }
    it { expect(subject).to have_db_column(:photo).of_type(:string) }
    it { expect(subject).to have_db_column(:price_for_room).of_type(:integer) }
    it { expect(subject).to have_db_column(:room_description) }
    it { expect(subject).to have_db_column(:address).of_type(:string) }
    it { expect(subject).to have_db_column(:status).of_type(:string) }
    it { expect(subject).to have_db_column(:author_id).of_type(:integer) }

    it { expect(subject).to have_db_index(:author_id) }
    it { expect(subject).to have_db_index(:title) }
  end

  describe 'relations' do
    it { expect(subject).to belong_to(:author).class_name('User').with_foreign_key('author_id') }
    it { expect(subject).to have_many(:comments) }
    it { expect(subject).to have_many(:raitings) }
    it { expect(subject).to have_many(:raters).through(:raitings) }
  end

  describe 'validation' do
    context 'rules' do
      it { expect(subject).to validate_presence_of :title }
      it { expect(subject).to validate_uniqueness_of :title }
      it { expect(subject).to ensure_length_of(:title).is_at_most(75) }
      it { expect(subject).to validate_numericality_of(:price_for_room) }
      it { expect(subject).to ensure_inclusion_of(:status)
                              .in_array(%w[pending approved rejected])
                              .on(:update) }
    end

    context 'with valid data' do
      it { expect(build :hotel).to be_valid }
    end

    context 'with invalid data' do
      it { expect(subject).not_to allow_value(nil).for(:title) }
      it { expect(subject).not_to allow_value(-5,95).for(:price_for_room) }
      it 'duplicate title' do
        hotel = create(:hotel)
        expect(subject).not_to allow_value(hotel.title).for(:title)
      end
    end
  end

  describe 'methods' do
    context 'status sets defaults on create' do
      let!(:hotel) { create :hotel }
      it { expect(hotel.status).to eq 'pending' }
    end
  end
end
