require 'rails_helper'

describe Comment do
  subject { described_class.new }

  context 'database contains' do
    it { expect(subject).to have_db_column(:body).of_type(:text) }
    it { expect(subject).to have_db_column(:user_id).of_type(:integer) }
    it { expect(subject).to have_db_column(:hotel_id).of_type(:integer) }
  end

  context 'relations' do
    it { expect(subject).to belong_to(:hotel) }
    it { expect(subject).to belong_to(:user) }
  end

  context 'validates' do
    context 'rules' do
      it { expect(subject).to validate_presence_of :body }
    end

    context 'with incorrect data' do
      it { expect(subject).not_to allow_value('').for(:body) }
    end

    context 'with correct data' do
      it { expect(build :comment).to be_valid }
    end
  end
end

