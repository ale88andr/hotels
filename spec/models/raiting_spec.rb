require 'rails_helper'

describe Raiting do
  subject { described_class.new }

  context 'database contains' do
    it { expect(subject).to have_db_column(:user_id).of_type(:integer) }
    it { expect(subject).to have_db_column(:hotel_id).of_type(:integer) }
    it { expect(subject).to have_db_column(:score).of_type(:integer) }
  end

  context 'relations' do
    it { expect(subject).to belong_to(:hotel) }
    it { expect(subject).to belong_to(:user) }
  end
end