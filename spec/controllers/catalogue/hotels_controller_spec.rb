require 'rails_helper'

describe Catalogue::HotelsController do
  describe 'user access' do
    before { sign_in create :user }

    describe 'GET #new' do
      before { get :new }

      it 'assigns a new Hotel to @hotel' do
        expect(assigns[:hotel]).to be_a_new Hotel
      end
      it 'render :new template' do
        expect(response).to render_template :new
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        subject { post :create, hotel: attributes_for(:hotel) }
        it 'saves new hotel in the database' do
          expect{
            subject
          }.to change(Hotel, :count).by(1)
        end
        it 'redirects to hotels index' do
          subject
          expect(response).to redirect_to catalogue_hotels_url
        end
        it 'set flash notice' do
          subject
          expect(flash[:notice]).to be
        end
      end

      context 'with invalid attributes' do
        subject { post :create, hotel: attributes_for(:hotel, title: nil) }
        it "don't saves user in the database" do
          expect{
            subject
          }.not_to change(Hotel, :count)
        end
        it 'redirects to :new action' do
          subject
          expect(response).to render_template :new
        end
        it 'set flash error message' do
          subject
          expect(flash[:error]).to be
        end
      end
    end
  end

  describe 'guest access' do
    describe 'GET #new' do
      before { get :new }

      it 'redirect to root' do
        expect(response).to redirect_to new_user_session_url
      end
    end
  end
end
