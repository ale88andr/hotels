require 'rails_helper'

describe Catalogue::CommentsController do
  describe 'user access' do
    before { sign_in create :user }

    describe 'POST #create' do
      let(:hotel) { create :hotel }
      context 'with valid attributes' do
        subject { post :create, comment: attributes_for(:comment, hotel_id: hotel.id) }
        it 'saves new hotel in the database' do
          expect{
            subject
          }.to change(Comment, :count).by(1)
        end
        it 'redirects to hotels index' do
          subject
          expect(response).to redirect_to catalogue_hotel_url(hotel)
        end
        it 'set flash notice' do
          subject
          expect(flash[:notice]).to be
        end
      end

      context 'with invalid attributes' do
        subject { post :create, comment: attributes_for(:comment, body: nil, hotel_id: hotel.id) }
        it "don't saves comment in the database" do
          expect{
            subject
          }.not_to change(Comment, :count)
        end
        it 'redirects to hotel' do
          subject
          expect(response).to redirect_to catalogue_hotel_path(hotel)
        end
        it 'set flash error message' do
          subject
          expect(flash[:error]).to be
        end
      end
    end
  end
end

