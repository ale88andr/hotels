require 'rails_helper'

RSpec.describe Catalogue::RaitingsController, type: :controller do
  let(:user)    { create :user }
  let!(:hotel)  { create :hotel, :with_author }
  let!(:current_user_hotel) { create :hotel, author_id: user.id }

  before { sign_in user }

  describe 'GET create' do
    context 'authored by some user' do
      subject { post :create, raiting: { hotel_id: hotel.id, score: 5 } }

      it 'assigns rate hotel to @hotel' do
        subject
        expect(assigns[:hotel]).to eq hotel
      end

      it 'saves rating to database' do
        expect{ subject }.to change(Raiting, :count)
      end

      context 'after save' do
        before { subject }

        it 'redirects to hotel' do
          expect(response).to redirect_to catalogue_hotel_url(hotel)
        end

        it 'sets flash' do
          expect(flash[:notice]).to be
        end
      end
    end

    context 'authored by current user' do
      before { post :create, raiting: { hotel_id: current_user_hotel.id, score: 5 } }

      it 'redirects to hotel' do
        expect(response).to redirect_to catalogue_hotel_url(current_user_hotel)
      end

      it 'sets flash alert' do
        expect(flash[:alert]).to be
      end
    end
  end

  describe 'GET update' do
    context 'authored by current user' do
      let!(:rate) { Raiting.create!(hotel_id: current_user_hotel.id, user_id: user.id, score: 1) }
      before { patch :update, raiting: { hotel_id: current_user_hotel.id, score: 5 }, id: rate.id }

      it 'redirects to hotel' do
        expect(response).to redirect_to catalogue_hotel_url(current_user_hotel)
      end

      it 'sets flash alert' do
        expect(flash[:alert]).to be
      end
    end

    context 'authored by some user' do
      let!(:rate) { Raiting.create!(hotel_id: hotel.id, user_id: user.id, score: 1) }
      before { put :update, raiting: { hotel_id: hotel.id, score: 5 }, id: rate.id }

      it 'saves rating to database' do
        expect( Raiting.find(rate.id).score ).to eq 5
      end

      it 'redirects to hotel' do
        expect(response).to redirect_to catalogue_hotel_url(hotel)
      end

      it 'sets flash' do
        expect(flash[:notice]).to be
      end
    end
  end

end
